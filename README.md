# sync-for-reddit v22
These are the patches for Sync For Reddit app.

## Why?
These patches perform the following:
* disable the piracy checker.
* remove ads.
* change Preload Images default to Wifi Only.

## Supported Version
The only supported version for now is `v22.4.16` i.e. `1298`. You can use aurora store to download using this code.

aapt2 output:
```
package: name='com.laurencedawson.reddit_sync' versionCode='1298' versionName='v22.4.16' platformBuildVersionName='' platformBuildVersionCode='' compileSdkVersion='32' compileSdkVersionCodename='12'
```


## How to apply?

The following are required to use the patch:
* Git bash / Unix pc with diff and patch tools
* Sync for Reddit v22 apk (version 22.4.16).
* apktool version 2.6.1 or above (jar). [download](https://github.com/iBotPeaches/Apktool)
* patches.patch file from this repo

Rename the apk to sync-for-reddit-v22.apk.
Rename your apktool to apktool.jar

make a folder and put sync-for-reddit-v22.apk and apktool.jar inside it.

First decompile the apk. Open a terminal and write the following
```
java -jar apktool.jar d sync-for-reddit-v22.apk -r -b
```

Then we patch the files using `patch` command.
```shell
patch -p0 < patches.patch
```

The app code should be patched. now build the apk and sign it.
```shell
# build using apktool
java -jar apktool.jar b sync-for-reddit-v22
```

Now to sign it we download user-apk-signer from [here on github](https://github.com/patrickfav/uber-apk-signer).

Rename it to signer.jar.

Then we run
```shell
java -jar signer.jar --apks sync-for-reddit-v22\dist\*
```

it should create a new file with `aligned-debugSigned` in its name.

install the apk and it should work.

### Errors on windows
Sometimes there's line ending errors like "Hunk #1 failed"

Follow this [stackoverflow answer](https://unix.stackexchange.com/questions/239364/how-to-fix-hunk-1-failed-at-1-different-line-endings-message). TLDR; use `dos2unix [filename]` and `unix2dos [filename]` to convert dos line endings to unix, and reverse.

## Summary 
Sync for Reddit is a Great app for browsing reddit, I'm thinking of buying it once its on sale.


Till then, Have Fun with Reddit!

## Credits
TestingCodeHere@discord